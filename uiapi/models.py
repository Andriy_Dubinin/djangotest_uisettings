from django.db import models
from djangotest.campaigns.models import Campaign


# Create your models here.

class UISettings(models.Model):
	class Meta():
		db_table = 'settings'

	name = models.CharField(max_length=200)
	value_integer = models.IntegerField(blank=True, null=True)
	value_float = models.FloatField(blank=True, null=True)
	value_string = models.TextField(blank=True, null=True)
	value_boolean = models.NullBooleanField()
	value_json = models.FileField(upload_to='uploads/')
	settings_campaign = models.ForeignKey(Campaign)

	def __unicode__(self):
		return self.name
